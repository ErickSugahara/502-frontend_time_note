import React from 'react';
import Home from './components/Home';

import './App.css';
import './global.css';


function App() {
  return (
    <>
      <Home />
    </>
  );
}

export default App;
