import React from 'react';

import './style.css';


const Input = ( { InputTitle, InputType, setInputValue }) => {

    const handleInputChange = (e) => {
        e.persist();     
        setInputValue(e.target.value);
    }
    return (
        <>  
            <div className="text-block">
                <span> {InputTitle}</span>
            </div>
            <input className="primary-input" onChange={handleInputChange} type={InputType} required/>
        </>
    );
}

export default Input;