import React, { useState } from 'react';
import Input from '../utils/Input';
import api from '../../services/api';

import './style.css';

const Login = () => {
    const [ user, setUser] = useState('');
    const [ pass, setPass] = useState('');

    const handleOnClick = async (e) => {
        e.preventDefault();
        const response = await api.get('/user', {
            query: {
                user,
                password: pass
            }
        });
        console.log(response);
        setUser('');
        setPass('');

    }

    return (
        <>

            <form className="form-block">
                <div className='login-title-block'>
                    <span className="login-title"> Log in </span>
                </div>
                <Input InputTitle="Usuário" setInputValue={setUser}/>
                <div className="recover-pass-block">
                    <a href="#" className="recover-pass">Esqueceu sua senha?</a>
                </div>
                <Input InputTitle="Senha" setInputValue={setPass} InputType="password" />
                <button className="btn" onClick={handleOnClick} type="submit">Login</button>
            </form>

        </>
    );
}

export default Login;
