import React from 'react';
import Login from '../Login';

import './style.css';

const Home = () => {
    return (
        <>  
            <div className="container">
                <Login />
            </div>
        </>
    );
}

export default Home;